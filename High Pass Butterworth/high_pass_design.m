%% AUTHOR    : Apostolos Fanakis
%% $DATE     : 16-Aug-2018 19:23:40 $
%% $Revision : 1.00 $
%% DEVELOPED : 9.0.0.341360 (R2016a)
%% FILENAME  : high_pass_design.m
%% AEM       : 8261
%%
%% ========== DESIGN SPECIFICATIONS START ==========
% Figures out design specifications according to my AEM number

AEM = [8 2 6 1];

if AEM(4)<4
	design_param_m = 1;
elseif AEM(3)<7
	design_param_m = 0;
else
	design_param_m = 3;
end

specification_pass_frequency = (4+design_param_m)*1000; % Hz
specification_pass_radial_frequency = ...
    specification_pass_frequency*(2*pi); % rad/s

specification_stop_frequency = specification_pass_frequency/2.6; % Hz
specification_stop_radial_frequency = ...
    specification_stop_frequency*(2*pi); % rad/s

specification_min_stop_attenuation = 24+AEM(4)*(6/9); % dB
specification_max_pass_attenuation = 0.5+AEM(3)/36; % dB

% Outputs results
fprintf(['\n' '===== DESIGN SPECIFICATIONS =====' '\n' ...
    'Filter design specifications:\n' ...
    'Pass frequency = %.3fHz = %.3frad/s\n' ...
    'Stop frequency = %.3fHz = %.3frad/s\n' ...
    'Min stop attenuation = %.3fdB\n' ...
    'Max pass attenuation = %.3fdB\n'], ...
    specification_pass_frequency, specification_pass_radial_frequency, ...
    specification_stop_frequency, specification_stop_radial_frequency, ...
    specification_min_stop_attenuation, specification_max_pass_attenuation);

clear design_param_m

% ========== DESIGN SPECIFICATIONS END ==========

%% ========== PROTOTYPE LOW PASS DESIGN SPECIFICATIONS START ==========
% Calculates the specifications for the low pass design that will later be
% converted to the desired high pass filter

% Calculates the specifications using the eq. 12-4
% prototype_normalized_pass_radial_frequency = 1; % rad/s
prototype_normalized_stop_radial_frequency = ...
    specification_pass_radial_frequency/ ...
    specification_stop_radial_frequency; % rad/s

% Min and max attenuations remain the same

% Outputs results
fprintf(['\n' '===== PROTOTYPE LOW PASS DESIGN SPECIFICATIONS =====' '\n' ...
    'The prototype low pass filter will be designed with the\n' ...
    'normalized stop radial frequency %.3frad/s, the normalized\n' ...
    'pass radial frequency is equal to 1rad/s.\n' ...
    'Min and max attenuation specifications remain the same.\n'], ...
    prototype_normalized_stop_radial_frequency);

% ========== PROTOTYPE LOW PASS DESIGN SPECIFICATIONS END ==========

%% ========== PROTOTYPE LOW PASS DESIGN START ==========
% Designs the prototype normalized filter.

% Calculates the filter's order using the eq. 9-52
temp_filter_order = log10(((10^ ...
    (specification_min_stop_attenuation/10)-1)/(10^ ...
    (specification_max_pass_attenuation/10)-1)))/ ...
    (2*log10(prototype_normalized_stop_radial_frequency));
design_filter_order = ceil(temp_filter_order);

% Calculates the frequency at which half power of the low pass prototype
% occurs using the eq. 9-48
low_pass_prototype_half_power_radial_frequency = 1/ ...
    (10^(specification_max_pass_attenuation/10)-1)^ ...
    (1/(2*design_filter_order)); % rad/s

% Transforms the result to get the corresponding frequency for the high
% pass using the eq. 12-3
design_half_power_radial_frequency = specification_pass_radial_frequency/ ...
    low_pass_prototype_half_power_radial_frequency; % rad/s

% -----
% Calculates stable poles, zeros, angles and other characteristic sizes
% using the Guillemin algorithm for a normalized low pass Butterworth
% filter.
%
% So for the time being we assume that the pass radial frequency is equal
% to unity (1).
% -----

% Initializes necessary variables
low_pass_prototype_number_of_poles = idivide(design_filter_order, ...
    int32(2),'ceil');
% Creates five vector arrays of dimensions [1 * number_of_poles] filled
% with zeros to store:
%   - the Butterworth angles with reference to the negative horizontal axes,
%   - the real parts of the poles,
%   - the imaginary parts of the poles,
%   - the radial frequencies (Omega0) of the poles and
%   - the Q's of the poles
design_butterworth_angles = zeros([1 low_pass_prototype_number_of_poles]);
low_pass_prototype_poles_real_parts = ...
    zeros([1 low_pass_prototype_number_of_poles]);
low_pass_prototype_poles_imaginary_parts = ...
    zeros([1 low_pass_prototype_number_of_poles]);
low_pass_prototype_poles_radial_frequencies = ...
    zeros([1 low_pass_prototype_number_of_poles]);
low_pass_prototype_poles_Q = zeros([1 low_pass_prototype_number_of_poles]);

% Calculates the Butterworth angles using the method suggested in chapter
% 9 (page 10) of the course notes and then uses them to calculate the
% Chebyshev poles
if mod(design_filter_order,2)~=0 % Odd number of poles
    % First pole has a zero angle
    design_butterworth_angles(1,1)=0;
    % The rest of the poles are scattered in the left half pane with
    % equal angles
    % Theta is a helper parameter
    theta=180/design_filter_order;
    
    % Calculates the first pole's real part using the eq. 9-31
    low_pass_prototype_poles_real_parts(1,1) = ...
        -cosd(design_butterworth_angles(1,1));
    % Calculates the first pole's imaginary part using the eq. 9-31
    low_pass_prototype_poles_imaginary_parts(1,1) =  ...
        sind(design_butterworth_angles(1,1));

    low_pass_prototype_poles_radial_frequencies(1,1) = 1;
    % Calculates the first pole's Q using the eq. 9-38
    low_pass_prototype_poles_Q(1,1) = 1/ ...
        (2*cosd(design_butterworth_angles(1,1)));
    
    % Calculates the rest of the poles in the same way
    for i=2:low_pass_prototype_number_of_poles
        design_butterworth_angles(1,i) = double((i-1)*theta);
        % Pole's real part, eq. 9-31
        low_pass_prototype_poles_real_parts(1,i) = ...
            -cosd(design_butterworth_angles(1,i));
        % Pole's imaginary part, eq. 9-31
        low_pass_prototype_poles_imaginary_parts(1,i) = ...
            sind(design_butterworth_angles(1,i));

        low_pass_prototype_poles_radial_frequencies(1,i) = 1;
        % Pole's Q, eq. 9-38
        low_pass_prototype_poles_Q(1,i) = 1/ ...
            (2*cosd(design_butterworth_angles(1,i)));
    end
else % Even number of poles
    % Theta is a helper parameter
    theta=90/low_pass_prototype_number_of_poles;

    for i=1:low_pass_prototype_number_of_poles
        design_butterworth_angles(1,i) = double(90)/ ...
            double(design_filter_order)+double((i-1)*theta);
        % Pole's real part, eq. 9-31
        low_pass_prototype_poles_real_parts(1,i) = ...
            -cosd(design_butterworth_angles(1,i));
        % Pole's imaginary part, eq. 9-31
        low_pass_prototype_poles_imaginary_parts(1,i) = ...
            sind(design_butterworth_angles(1,i));

        low_pass_prototype_poles_radial_frequencies(1,i) = 1;
        % Pole's Q, eq. 9-106
        low_pass_prototype_poles_Q(1,i) = 1/ ...
            (2*cosd(design_butterworth_angles(1,i)));
    end
end

% Outputs results
fprintf(['\n' '===== PROTOTYPE LOW PASS DESIGN =====' '\n' ...
    'A prototype low pass Butterworth filter is designed with the\n' ...
    'specifications previously calculated.\n\n' ...
    'Filter order = %.3f\n' ...
    'Filter order ceiling = %d\n' ...
    'Radial frequency at which half power occurs = %.3frad/s\n' ...
    'Butterworth angles are ' char(177) '%.2f' char(176) ' and ' ...
    char(177) '%.2f' char(176) '\n'], ...
    temp_filter_order, design_filter_order, ...
    design_half_power_radial_frequency, design_butterworth_angles(1,1), ...
    design_butterworth_angles(1,2));

fprintf('\nLow pass Butterworth poles found:\n');
for i=1:low_pass_prototype_number_of_poles
    fprintf(['Pole %d:\t' '%.3f' char(177) ...
        '%.3fi, radial frequency = %.3f, Q = %.3f\n'], ...
        i, low_pass_prototype_poles_real_parts(1,i), ...
        low_pass_prototype_poles_imaginary_parts(1,i), ...
        low_pass_prototype_poles_radial_frequencies(1,i), ...
        low_pass_prototype_poles_Q(1,i));
end

% Clears unneeded variables from workspace
clearVars = {'i', 'prototype_normalized_stop_radial_frequency', 'theta', ...
    'low_pass_prototype_half_power_radial_frequency', 'temp_filter_order'};
clear(clearVars{:})
clear clearVars

% ========== PROTOTYPE LOW PASS DESIGN END ==========

%% ========== POLES TRANSFORMATION START ==========
% Transforms the prototype's poles

% Initializes necessary variables
% Calculates the number of poles that will occur after the transformation
high_pass_number_of_poles = low_pass_prototype_number_of_poles;

% According to the course notes (chapter 12, end of page 5) the
% transformation leaves the poles unchanged.
high_pass_poles_real_parts = low_pass_prototype_poles_real_parts;
high_pass_poles_imaginary_parts = ...
    low_pass_prototype_poles_imaginary_parts;
high_pass_poles_radial_frequencies = ...
    low_pass_prototype_poles_radial_frequencies;
high_pass_poles_Q = low_pass_prototype_poles_Q;

% The transormation also produces a number of zeros at (0,0) equal to the
% filter order
high_pass_transfer_function_zeros = zeros([1 design_filter_order]);

% Outputs results
fprintf(['\n' '===== LOW PASS TO HIGH PASS TRANSFORMATION =====' '\n' ...
    'The prototype low pass Butterworth filter is transformed into\n' ...
    'a high pass Butterworth using the transformation S = 1/s.\n']);

fprintf('\nHigh pass Butterworth normalized poles found:\n');
for i=1:high_pass_number_of_poles
    fprintf(['Pole %d:\t' '%.3f' char(177) ...
        '%.3fi, radial frequency = %.3f, Q = %.3f\n'], ...
        i, high_pass_poles_real_parts(1,i), ...
        high_pass_poles_imaginary_parts(1,i), ...
        high_pass_poles_radial_frequencies(1,i), ...
        high_pass_poles_Q(1,i));
end

fprintf('\nTransfer function zeros:\n');
for i=1:length(high_pass_transfer_function_zeros)
    fprintf(['Zero %d:\t' '0%+.3fi\n'], ...
        i, high_pass_transfer_function_zeros(1,i));
end

% Clears unneeded variables from workspace
clear low_pass_prototype_number_of_poles
clear -regexp ^low_pass_prototype_

% ========== POLES TRANSFORMATION END ==========

%% ========== POLES DE-NORMALIZATION START ==========
% The high pass filter poles calculated above are those of a normalized
% filter. De-normalization is needed to get the actual poles for the
% desired high pass filter.

for i=1:high_pass_number_of_poles
    high_pass_poles_real_parts(1,i) = high_pass_poles_real_parts(1,i)* ...
        design_half_power_radial_frequency;
    high_pass_poles_imaginary_parts(1,i) = ...
        high_pass_poles_imaginary_parts(1,i)* ...
        design_half_power_radial_frequency;
    high_pass_poles_radial_frequencies(1,i) = ...
        design_half_power_radial_frequency;
end

% Outputs results
fprintf(['\n' '===== POLES DE-NORMALIZATION =====' '\n' ...
    'The high pass filter designed is normalized.\n' ...
    'The poles are de-normalized.\n']);

fprintf('\nHigh pass Butterworth poles found:\n');
for i=1:high_pass_number_of_poles
    fprintf(['Pole %d:\t' '%.3f' char(177) ...
        '%.3fi, radial frequency = %.3f, Q = %.3f\n'], ...
        i, high_pass_poles_real_parts(1,i), ...
        high_pass_poles_imaginary_parts(1,i), ...
        high_pass_poles_radial_frequencies(1,i), ...
        high_pass_poles_Q(1,i));
end

% Clears unneeded variables from workspace
clear i
clear -regexp ^transformation_

% ========== POLES DE-NORMALIZATION END ==========

%% ========== UNITS IMPLEMENTATION START ==========
% AEM(2) = 2, so the first design strategy is going to be used in the
% Sallen-Key high pass circuits.

% -------------------------------------------------------------------------
% Unit 1 has a pole pair with Q equal to 0.5412 and lies on a circle with a
% radius equal to 25097.78.
% -------------------------------------------------------------------------
% Unit 1 has a pole pair with Q equal to 1.3066 and lies on a circle with a
% radius equal to 25097.78.
% -------------------------------------------------------------------------

% Initializes necessary arrays, each array is 1X2, the first element (1,1)
% corresponds to the first unit and the second element (1,2) to second
% unit.
units_R = ones([1 high_pass_number_of_poles]);
units_C = ones([1 high_pass_number_of_poles]);
units_r1 = ones([1 high_pass_number_of_poles]);
units_r2 = zeros([1 high_pass_number_of_poles]);
units_k = zeros([1 high_pass_number_of_poles]);
units_frequency_scale_factors = zeros([1 2]);
units_amplitude_scale_factors = zeros([1 2]);
units_transfer_functions = [tf(1) tf(1)];

for i=1:high_pass_number_of_poles
    % Calculates k and r2 using the eq. 6-75
    units_r2(1,i) = 2-1/high_pass_poles_Q(1,i); % Ohm
    units_k(1,i) = 3-1/high_pass_poles_Q(1,i);
    
    % Selects the appropriate frequency scale factor to transfer the
    % normalized radial frequency back to the original
    units_frequency_scale_factors(1,i) = ...
        high_pass_poles_radial_frequencies(1,i);
    % AEM(3) = 6, so the magnitude scaling will be performed to achieve a
    % capacitor value of 0.1uF using the eq. 6-33
    units_amplitude_scale_factors(1,i) = ...
        units_C(1,i)/ ...
        (units_frequency_scale_factors(1,i)*0.1*10^(-6));
    
    % Performs scaling
    units_R(1,i) = units_R(1,i)* ...
        units_amplitude_scale_factors(1,i); % Ohm
    units_C(1,i) = 0.1*10^(-6); % Farad
    units_r1(1,i) = units_r1(1,i)* ...
        units_amplitude_scale_factors(1,i); % Ohm
    units_r2(1,i) = units_r2(1,i)* ...
        units_amplitude_scale_factors(1,i); % Ohm
    
    % Builds unit's transfer function
    % Builds numerator and denominator of the transfer function using the
    % eq. 6-68
    temp_G = (units_R(1,i)+units_r2(1,i))/units_R(1,i);
    unit_numerator = [temp_G ...
        0 ...
        0];
    unit_denominator = [1 ...
        2/(units_C(1,i)*units_R(1,i))+(1-temp_G)/(units_C(1,i)*units_R(1,i)) ...
        1/(units_C(1,i)^2*units_R(1,i)^2)];

    units_transfer_functions(1,i) = ...
        tf(unit_numerator, unit_denominator);
end

% Outputs results
fprintf(['\n' '===== UNITS IMPLEMENTATION =====' '\n' ...
    'Units implementation details:\n']);
for i=1:high_pass_number_of_poles
    fprintf(['Unit %d:\n' ...
        '\tPole radial frequency = %.3f\n'...
        '\tPole Q = %.3f\n' ...
        '\tTransfer function zero = 0%+.3fi (double)\n' ...
        '\tCircuit elements:\n' ...
        '\t\tR1 = %.3fOhm\n' ...
        '\t\tR2 = %.3fOhm\n' ...
        '\t\tr1 = %.3fOhm\n' ...
        '\t\tr2 = %.3fOhm\n' ...
        '\t\tC1 = %.7fF\n' ...
        '\t\tC2 = %.7fF\n'], ...
        i, high_pass_poles_radial_frequencies(1,i), ...
        high_pass_poles_Q(1,i), ...
        high_pass_transfer_function_zeros(1,i), ...
        units_R(1,i), units_R(1,i), units_r1(1,i), units_r2(1,i), ...
        units_C(1,i), units_C(1,i));
end

% Clears unneeded variables from workspace
clearVars = {'i', 'temp_G', 'unit_numerator', 'unit_denominator'};
clear(clearVars{:})
clear clearVars

% ========== UNITS IMPLEMENTATION END ==========

%% ========== GAIN ADJUSTMENT START ==========

total_gain_high = units_k(1,1)*units_k(1,2);
unit_adjustment_gain = (10^(10/20))/total_gain_high;
% We arbitrarily choose to use a 10KOhm series resistor in the adjustment
% unit
unit_adjustment_feedback_resistor = 10*10^3*unit_adjustment_gain;

% ========== GAIN ADJUSTMENT END ==========

%% ========== TRANSFER FUNCTIONS STUDY START ==========

total_transfer_function = series(units_transfer_functions(1,1), ...
    units_transfer_functions(1,2));
total_transfer_function = total_transfer_function*unit_adjustment_gain;

% Arbitrary high frequency to display the gain at high frequencies
high_frequency = 90000; % Hz

% Plots each unit's frequency response
for i=1:high_pass_number_of_poles
    plot_transfer_function(units_transfer_functions(1,i), ...
        (high_frequency));
end

% Plots the total filter frequency response
plot_transfer_function(total_transfer_function, ...
    [specification_stop_frequency ...
    design_half_power_radial_frequency/(2*pi) ...
    specification_pass_frequency ...
    high_frequency]);

% Plots the total filter attenuation function
plot_transfer_function(inv(total_transfer_function), ...
    [specification_stop_frequency ...
    design_half_power_radial_frequency/(2*pi) ...
    specification_pass_frequency ...
    high_frequency]);

plot_transfer_function(inv(total_transfer_function/unit_adjustment_gain), ...
    [specification_stop_frequency ...
    design_half_power_radial_frequency/(2*pi) ...
    specification_pass_frequency ...
    high_frequency]);

%{
ltiview('bodemag', units_transfer_functions(1,1));
ltiview('bodemag', units_transfer_functions(1,2));
ltiview('bodemag', total_transfer_function);
%}
ltiview('bodemag', units_transfer_functions(1,1), units_transfer_functions(1,2), ...
    total_transfer_function);

hold off

sampling_time_seconds = 60; % s
sampling_frequency_Fs = 80000; % Hz
sampling_period_T = 1/sampling_frequency_Fs; % s
sampling_time_vector = 0:sampling_period_T: ...
    sampling_time_seconds-sampling_period_T;
sampling_length_L = length(sampling_time_vector);
frequency_vector = sampling_frequency_Fs/sampling_length_L* ...
    (0:(sampling_length_L/2));

input_cos_signal_frequency_1 = 0.2*specification_stop_radial_frequency;
input_cos_signal_frequency_2 = 0.7*specification_stop_radial_frequency;
input_cos_signal_frequency_3 = 1.6*specification_pass_radial_frequency;
input_cos_signal_frequency_4 = 2.4*specification_pass_radial_frequency;
input_cos_signal_frequency_5 = 3.5*specification_pass_radial_frequency;

input_signal = cos(input_cos_signal_frequency_1*sampling_time_vector)+ ...
    0.6*cos(input_cos_signal_frequency_2*sampling_time_vector)+ ...
    1.5*cos(input_cos_signal_frequency_3*sampling_time_vector)+ ...
    0.7*cos(input_cos_signal_frequency_4*sampling_time_vector)+ ...
    0.4*cos(input_cos_signal_frequency_5*sampling_time_vector);

system_output = lsim(total_transfer_function, input_signal, ...
    sampling_time_vector);

% Plots only the first 500 samples
figure('Name','Filter response to input signal');
title('Filter response to input signal');
plot(sampling_time_vector(1:500), input_signal(1:500), ...
    sampling_time_vector(1:500), system_output(1:500));
title('Filter response to input signal');
xlabel('Time (s)');
ylabel('Voltage (V)');
grid on

% Plots the power spectrum of the input signal
input_signal_fft = fft(input_signal);
Pyy = input_signal_fft.*conj(input_signal_fft)/sampling_length_L;
figure('Name','Input signal spectrum');
semilogx(frequency_vector,Pyy(1:sampling_length_L/2+1))
title('Input signal spectrum');
xlabel('Frequency (Hz)');
ylabel('Trivial unit');
grid on

% Plots the power spectrum of the output signal
system_output_fft = fft(system_output, length(sampling_time_vector));
Pyy = system_output_fft.*conj(system_output_fft)/sampling_length_L;
figure('Name','Output signal spectrum');
semilogx(frequency_vector,Pyy(1:sampling_length_L/2+1))
title('Output signal spectrum');
xlabel('Frequency (Hz)');
ylabel('Trivial unit');
grid on

% Clears unneeded variable from workspace
clearVars = {'high_frequency', 'total_transfer_function', 'Pyy', ...
    'frequency_vector', 'system_output', 'system_output_fft'};
clear(clearVars{:})
clear clearVars
clear -regexp _transfer_functions$
clear -regexp ^sampling_
clear -regexp ^input_

% ========== TRANSFER FUNCTIONS STUDY END ==========